<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    
    <html lang="pt-BR">
    
    <link type="text/css" href="<c:url value="css/index.css" />" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
        <script src="https://www.gstatic.com/firebasejs/4.3.0/firebase.js"></script>
<script>
  // Initialize Firebase
  
  var config = {
    apiKey: "AIzaSyBYfNZR1lZCEGkAmo5XbexPlUVNSRnOP0g",
    authDomain: "ToDoList2-52030.firebaseapp.com",
    databaseURL: "https://ToDoList2-52030.firebaseio.com",
    projectId: "ToDoList2-52030",
    storageBucket: "ToDoList2-52030.appspot.com",
    messagingSenderId: "1034571877529"
  };
  firebase.initializeApp(config);
</script>
<body>
    <div id="login">
    <div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>Login</h1>
			</div>

    	
    	<div class="login-form">
				<div class="control-group">
				<input type="text" autofocus id="nome"  placeholder="Nome" required="required"/>
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>
				
				<div class="control-group">
				<input type="email" id="email" placeholder="Email" required="required"/>
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>
				
				<div class="control-group">
				<input type="password"  id="senha" placeholder="Senha" required="required"/>
				<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<a class="btn btn-primary btn-large btn-block" id="btnEntrar" onClick="verificar()">Logar</a><br/>
				<a class="btn btn-primary btn-large btn-block" id="btnEntrarGoogle" onClick="logarComGoogle()">Logar com o GOOGLE</a>
				<a class="login-link" id="linkCadastrar" onclick="cadastrar()">Cadastrar-se</a>
			</div>
			
			
      
         	<div id="loginInvalido">

	 			<p>Usuario ou senha inválidos</p>
	 		</div>
    </div>
    </div>
    </div>


</body>
    <script type="text/javascript">

    
    function sigIn(provider){
    	firebase.auth()
    	.signInWithPopup(provider)
    	.then(function(result){
    		console.log(result);
    		
    	}).fail(function(error){
    		console.log(error);
    	});
    }
    
    function logarComGoogle(){
    	var provider = new firebase.auth.GoogleAuthProvider();
    	firebase.auth()
    	.signInWithPopup(provider)
    	.then(function(result){
    		console.log(result);
    		console.log(result.user.email);
    		
    		var usuarioObj = {
        			"email": result.user.email,
        	}
        	
        	 $.ajax({
                 url: "http://localhost/ToDoList2/buscar/usuario",
                 dataType: "json",
                 method: "POST",
                 contentType: "application/json",
                 data: JSON.stringify ( usuarioObj )

         }).done(function(response, textstatus, teste){
             console.log("teste = " + teste.status);
             console.log("response = " + response);
             obj_token = response.token;
     		 obj_usuario = response.usuario;
     		 localStorage.clear();
     		 localStorage.setItem("token", obj_token.token);
     		 localStorage.setItem("nome_usuario", obj_usuario.nome);
             carregarProximaPagina();
             }).fail(function(response,textstatus,teste){	
                 $("#nome").css({"display":"none"});
                 $("#email").css({"display":"none"});
                 $("#email").val(result.user.email);
                 $("#nome").val(result.user.displayName);
                 $("#titulo").text("Bem vindo "+ result.user.displayName+", insira uma senha para continuar");
                 $("#senha").val("");
                 $("#btnEntrar").text("Cadastrar");
                 $("#btnEntrarGoogle").css({"display":"none"});
                 $("#linkCadastrar").css({"display":"none"});
         		
             })

    		
    	}).fail(function(error){
    		console.log(error);
    	});
		
    }
        function verificar(){
        console.log("Cheguei aqui");
        	if(($("#btnEntrar").text() === "Logar")){
        		
        		logar();
        	}else{
                inserir();	
        	}
        }
        
        function logar(){
        	var usuarioObj = {
        			"email": $("#email").val(),
        			"senha": $("#senha").val(),
        	}
        	
        	 $.ajax({
                 url: "http://localhost/ToDoList2/login/",
                 dataType: "json",
                 method: "POST",
                 contentType: "application/json",
                 data: JSON.stringify ( usuarioObj )

         }).done(function(response, textstatus, teste){
             console.log("teste = " + teste.status);
             console.log("response = " + response);
             obj_token = response.token;
     		 obj_usuario = response.usuario;
     		 localStorage.clear();
     		 localStorage.setItem("token", obj_token.token);
     		 localStorage.setItem("nome_usuario", obj_usuario.nome);
             carregarProximaPagina();
             }).fail(function(response,textstatus,teste){
            	console.log(teste.status);
         		console.log(response.status);
         		console.log("CODIGO = " + textstatus);
         		console.log("codigo fail");
         		$('#loginInvalido').css({"display":"block","color":"red"});
         		
             })
        }
        
        function carregarProximaPagina(){
        	$(window.location.href = "tarefas.jsp").fadeIn('500');
        }
        
        function cadastrar(){
        	$("#btnEntrar").text("Cadastrar");
        	$("#titulo").text("Cadastro");
        	document.getElementById("linkCadastrar").style.display="none";
        	$("#nome").css({"display": "block"});
        }
        /*
        Método que inseri o usuario
        */
        
    function inserir(){
        var usuarioObj = {
        "nome": $("#nome").val(),
        "email": $("#email").val(),
        "senha": $("#senha").val()
        };
        $.ajax({
            url: "http://localhost/ToDoList2/usuario/",
            dataType: "json",
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify ( usuarioObj )

    }).done(function(response, textstatus, teste){
        console.log("teste = " + teste.status);
        console.log("response = " + response);

        $("#nome").css({"display":"none"});
        $("#titulo").text("Login");
        $("#senha").val("");
        $("#btnEntrar").text("Logar");
        }).fail(function(response,textstatus,teste){
            console.log("status " + teste.status)
        })
    }
    </script>
</html>
