<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <c:url value="/" var="raiz" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-BR">
<head>
    <link type="text/css" href="<c:url value="css/tarefas.css" />" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tarefas</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div id="menu">
<ul>
  <li><a id="nomeUsuario"></a></li>
  <li style="float:right"><a id="sair">Sair</a></li>
</ul>
</div>
<div id="formularioTarefa">
	<form method="post" enctype="multipart/form-data">
		<input type="hidden" id="idTarefa"/>
		 <fieldset>
	        <fieldset class="grupo">
	            <div class="campo">
	                <label for="nome">Titulo</label>
	                <input type="text" id="titulo" autofocus style="width: 10em" required="required"/>
	            </div>
	            <div class="campo">
	                <label for="snome">Prioridade</label>
					<input type="number" id="prioridade" max="5" style="width: 10em" required="required"/>
	            </div>
	        </fieldset>
			<p><input type="file" id="btnAdicionarImagem" multiple = "multiple"/></p>
	         <div class="campo">
	            <label for="mensagem">Descri��o</label>
	            <textarea rows="6" cols="47" id="descricao"></textarea>
	            <label id="usuarioQueFinalizou"></label>
	        </div>
	        
		<button type="button" id="btnSalvar" onclick="verificar()" class="botao">Salvar</button>
		<button type="button" id="btnDeletar" onclick="deletar()" class="botao">Deletar</button>
		<button type="button" id="btnConcluir" class="botao">Concluir</button>
	       </fieldset>
	
	</form>
</div>

<div id="tabelaTarefas">
<div id="listaTarefas">
	<table id="lista" class="table" >
				<thead>
					<tr>
						<th>Titulo</th>
						<th>Prioridade</th>
						<th>Status</th>
						<th>Autor</th>
					</tr>
				</thead>
				<tbody id="corpoTabela">
				
				</tbody>
			</table>
</div>
</div>
</body>
<script type="text/javascript">

$(document).ready(function(){
	if (localStorage.getItem("token") == null) {
		$(window.location.href = "index.jsp").fadeIn('500');
	}else{

		 $("#nomeUsuario").text("Bem vindo "+localStorage.getItem("nome_usuario"));
		$("#btnDeletar").css({"display": "none"});
		$("#btnConcluir").css({"display": "none"});
		
		carregarTarefas();	
	}
})


$("#sair").click(function(){
	localStorage.clear();
	$(location).attr("href" , "index.jsp" );
})

$("#btnConcluir").click(function(){
	$("#btnConcluir").text("Concluida");
	alterar();
})

/*
 * M�todo que verifica se � um cadastro ou altera��o
 */
function verificar(){
	if ($("#idTarefa").val() === '') {
		console.log($("#idTarefa").val());
		console.log("Vou inserir");
		inserir();
	}else{
		console.log($("#idTarefa").val());
		alterar();
	}
}


/*
 * M�todo de inser��o de tarefas
 */
	function inserir(){
	    var tarefaObj = {
	    "titulo": $("#titulo").val(),
	    "descricao": $("#descricao").val(),
	    "prioridade": $("#prioridade").val()
	    };
	    $.ajax({
	        url: "http://localhost/ToDoList2/tarefa/",
	        dataType: "json",
	        method: "POST",
	        contentType: "application/json",
	        data: JSON.stringify ( tarefaObj ),
	        headers : {					
				"Authorization" : localStorage.getItem("token")
			},
	}).done(function(response, textstatus, teste){
	    console.log("teste = " + teste.status);
	    console.log("response = " + response);

	    upload(response);
	    $("#titulo").val("");
	    $("#descricao").val("");
	    $("#prioridade").val("");
	    $("#btnAdicionarImagem").val("");
		$('#usuarioQueFinalizou').css({"display":"none"});
	
	    carregarTarefas();
	    }).fail(function(response,textstatus,teste){
	        console.log("status " + teste.status)
	    })
	}
	
	
	/*
	Upload de anexos
	*/
	
	
	function upload(response){
		formData = new FormData();
		inputFile = document.getElementById("btnAdicionarImagem");					

		console.log("Valor do bot�o");
		console.log($("#btnAdicionarImagem").val());
		var cont=0;
		$(inputFile.files).each(function(){
			formData.append('file', $(this)[cont]);
			cont++;
		});
		cont=0;
		console.log("Antes do event");
		$(formData).each(function() {
			console.log($(this));
			console.log("Dentro do each");
			formData.append('file', $(this)[cont]);
			cont++;
			console.log($(this));
			
		});
		console.log(JSON.stringify(formData));
		$.ajax({
			url : "http://localhost/ToDoList2/tarefas/"+response.id,
			method : 'POST',
			data : formData,
			processData : false,
			contentType : false,
			cache : false,				
			headers : {					
				"Authorization" : localStorage.getItem("token")
			},
		}).done(function(response, textstatus, teste){
	    console.log("teste = " + teste.status);
	    console.log("response = " + response);

	    $("#titulo").val("");
	    $("#descricao").val("");
	    $("#prioridade").val("");
	    $("#btnAdicionarImagem").val("");
	
	    carregarTarefas();
	    }).fail(function(response,textstatus,teste){
	        console.log("status " + teste.status)
	    })
	}
	
	function alterar(){
		var tarefaObj;
		if ($("#btnConcluir").text()==='Concluida') {
			tarefaObj = {
					"id": $("#idTarefa").val(),
				    "titulo": $("#titulo").val(),
				    "descricao": $("#descricao").val(),
				    "prioridade": $("#prioridade").val(),
				    "status": true
				    };
		}else{
			tarefaObj = {
					"id": $("#idTarefa").val(),
				    "titulo": $("#titulo").val(),
				    "descricao": $("#descricao").val(),
				    "prioridade": $("#prioridade").val()
				    };	
		}
		console.log($("#titulo").val());
		console.log($("#descricao").val());
		console.log($("#prioridade").val());
			    $.ajax({
			        url: "http://localhost/ToDoList2/tarefa/",
			        dataType: "json",
			        method: "PUT",
			        contentType: "application/json",
			        data: JSON.stringify ( tarefaObj ),
			        headers : {					
						"Authorization" : localStorage.getItem("token")
					},
			}).done(function(response, textstatus, teste){
			    console.log("teste = " + teste.status);
			    console.log("response = " + response);
			    
			    $("#titulo").val("");
			    $("#descricao").val("");
			    $("#prioridade").val("");
			    $("#idTarefa").val("");
			    $("#btnAdicionarImagem").val("");

				$('#usuarioQueFinalizou').css({"display":"none"});
			    $("#btnSalvar").text("Salvar");
			    $("#btnDeletar").css({"display":"none"});
			    $("#btnConcluir").css({"display":"none"});
			    carregarTarefas();
			    }).fail(function(response,textstatus,teste){
			        console.log("status " + teste.status)
			    })
	}
	
	function deletar(){
		console.log($("#idTarefa").val());
		$.ajax({
			        url: "http://localhost/ToDoList2/tarefa/"+$("#idTarefa").val(),
			        dataType: "json",
			        method: "DELETE",
			        contentType: "application/json",
			        headers : {					
						"Authorization" : localStorage.getItem("token")
					},
			}).done(function(response, textstatus, teste){
			    console.log("teste = " + teste.status);
			    console.log("response = " + response);

				$("#titulo").attr('disabled', false);
			    $("#titulo").val("");

				$("#descricao").attr('disabled', false);
			    $("#descricao").val("");

				$('#usuarioQueFinalizou').css({"display":"none"});
				
				$("#prioridade").attr('disabled', false);
			    $("#prioridade").val("");

				$("#btnSalvar").attr('disabled', false);
			    $("#btnSalvar").text("Salvar");
			    $("#btnDeletar").css({"display":"none"});
			    $("#btnConcluir").css({"display":"none"});
			    carregarTarefas();
			    }).fail(function(response,textstatus,teste){
			        console.log("status " + teste.status)
			    })
	}
	
	function escapeHtml(unsafe) {
	    return unsafe
	         .replace(/&/g, "&amp;")
	         .replace(/</g, "&lt;")
	         .replace(/>/g, "&gt;")
	         .replace(/"/g, "&quot;")
	         .replace(/'/g, "&#039;");
	 }
	
	
	function buscarTarefa(t){
			console.log("BuscarTarefa");
			localStorage.setItem("busca", t.attributes["data-id"].value);
			console.log("Vou para o buscar");
			setTimeout(buscar(), 500);
		
	}
	
	function buscar(){
		console.log("Cheguei aqui");
		$.ajax({
			url : "http://localhost/ToDoList2/tarefa/" + localStorage.getItem("busca"),
			type : 'GET',	
			contentType : "application/json",
			headers : {					
				"Authorization" : localStorage.getItem("token")
			}
		}).done(function(response, textstatus, teste) {
			if (response.status===true) {

				$("#idTarefa").val(response.id);
				//Desabilitando o input
				$("#titulo").attr('disabled', true);
				$("#titulo").val(response.titulo);
				
				$("#prioridade").attr('disabled', true);
				$('#prioridade').val(response.prioridade);

				$('#usuarioQueFinalizou').css({"display":"block"});
				$('#usuarioQueFinalizou').text("Usuario que marcou como finalizada: "+localStorage.getItem("nome_usuario"));
				
				
				$("#descricao").attr('disabled', true);
				$("#descricao").val(response.descricao);
				
				$("#btnSalvar").attr('disabled', true);
				$("#btnSalvar").text("Alterar");
				$("#btnSalvar").css({"float":"left"});
				$("#btnDeletar").css({"display": "block","float":"left"});
				
				$("#btnConcluir").attr('disabled', true);
				$("#btnConcluir").text("Concluida");
				$("#btnConcluir").css({"display": "block","float":"left"});
			}else{
			
			$("#idTarefa").val(response.id);
			
			$("#titulo").attr('disabled', false);
			$("#titulo").val(response.titulo);

			$('#usuarioQueFinalizou').css({"display":"none"});
			$("#prioridade").attr('disabled', false);
			$('#prioridade').val(response.prioridade);
			
			$("#descricao").attr('disabled', false);
			$("#descricao").val(response.descricao);
			$("#btnSalvar").text("Alterar");
			
			$("#btnSalvar").attr('disabled', false);
			$("#btnSalvar").css({"float":"left"});
			$("#btnDeletar").css({"display": "block","float":"left"});

			
			$("#btnConcluir").attr('disabled', false);
			$("#btnConcluir").text("Concluir");
			$("#btnConcluir").css({"display": "block","float":"left"});
			
			}

		}).fail(function(response, textstatus, teste) {
			console.log('STATUS ' + textstatus);
			console.log(response);

		});
	}
	
	function carregarTarefas(){
		 $('#lista tbody').slideUp('fast');
		 
			var tdHTML = '';
			var trHTML = ''; 
			var empresas;
			
			$.ajax({
				url : "http://localhost/ToDoList2/tarefa/lista/",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				headers : {
				"Authorization" : localStorage.getItem("token")
				},
				
			}).done(function(response) {
				$('#lista tbody').empty();
				//conta quantos objetos tem no array
				$.each(response,function(index) {
					//devolve as informa��es de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psi��o 2
					// e o atributo desejada
					//var tarefa = response[index].empresa;
					console.log(response[index].tarefa.titulo);
					tdHTML = '<td>' + escapeHtml(response[index].tarefa.titulo) + '</td>' + '<td>' + response[index].tarefa.prioridade + '</td>'+'<td>' + response[index].status+'</td>'+'<td>' + response[index].nomeUsuario+'</td>' ;
					
					
					/* Listar a empresa com imagem */
					//tdHTML += '<td>' + "<img class='imagemEmpresa' src=" + "http://192.168.2.226/baseDeConhecimento" + response[index].imagem +  ">" + '</td>'; 				
					trHTML = '<tr data-id=' + response[index].tarefa.id + " onclick='buscarTarefa(this)'>" + tdHTML + '</tr>';
					//empresas = '';
					$('#lista tbody').append(trHTML).slideDown();
				});
		});
	 }
</script>

</html>