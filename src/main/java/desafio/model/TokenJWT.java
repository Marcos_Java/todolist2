package desafio.model;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.auth0.jwt.JWTSigner;

public class TokenJWT {
	private String token;
	public static final String EMISSOR = "Marcos";
	public static final String SECRET = "D35Af10";
	

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	 public static TokenJWT gerarToken(Usuario usuario, HttpServletRequest request){
		
		HashMap<String, Object> claims = new HashMap<String, Object>();
		claims.put("iss", EMISSOR);
		claims.put("id_usuario", usuario.getId());
		claims.put("email_usuario", usuario.getEmail());

		// hora atual em segundos
		long horaAtual = System.currentTimeMillis() / 1000;
		// expira��o do token (3600 segundos)
		long horaExpiracao = horaAtual + 86400;
		
		//teste 
		System.out.println("Hora Atual : "+ horaAtual);
		
		System.out.println("Hora expira��o"+ horaExpiracao);
		

		claims.put("iat", horaAtual);
		claims.put("exp", horaExpiracao);
		JWTSigner signer = new JWTSigner(SECRET);
		TokenJWT tokenJwt = new TokenJWT();
		tokenJwt.setToken(signer.sign(claims));
		
		return tokenJwt;
		
	}

}
