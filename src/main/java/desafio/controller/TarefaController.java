package desafio.controller;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWTVerifier;

import desafio.dao.DAOTarefa;
import desafio.dao.DAOUsuario;
import desafio.model.Anexos;
import desafio.model.Tarefa;
import desafio.model.TokenJWT;
import desafio.model.Usuario;
import desafio.view.InfoTarefa;

@RestController
public class TarefaController {

	@Autowired
	private DAOTarefa daoTarefa;

	@Autowired
	private DAOUsuario daoUsuario;
	
	@Autowired
	private ServletContext servletContext;
	


	@RequestMapping(value = "/tarefa/{id}", method = RequestMethod.POST)
	public ResponseEntity<FileInfo> uploadArquivo(@RequestParam("file") MultipartFile[] inputFile,
			@PathVariable Long id) {
		FileInfo fileInfo = new FileInfo();
		HttpHeaders headers = new HttpHeaders();

		if (inputFile.length != 0 && id != null) {
			try {

				// Cria a pasta da empresa no servidor caso ela n�o exista.
				String caminhoDiretorio = servletContext.getRealPath("/");
				String realPath = servletContext.getRealPath("/");
				File file = new File(realPath+"/anexos");
				if (file.mkdirs() == false) {
					file.mkdirs();	
				}
				// busca o procedimento
				Tarefa tarefa = daoTarefa.buscar(id);
				List<Anexos> anexos = new ArrayList<>();
				System.out.println("PROXIMA LINHA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				System.out.println("Tamanho do multipart >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + inputFile.length);
				for (int i = 0; i < inputFile.length; i++) {

					String caminhoimagem = "/anexos" + id + "." + i + ".jpg";

					File destinationFile = new File(caminhoDiretorio + caminhoimagem);

				
					inputFile[i].transferTo(destinationFile);

					Anexos anexo = new Anexos();
					anexo.setCaminho(caminhoimagem);

					anexos.add(anexo);
				}

				tarefa.setAnexos(anexos);
				daoTarefa.alterar(tarefa);

				return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<FileInfo>(HttpStatus.OK);
		}
	}

	
	
	
	@RequestMapping(value = "/tarefas/{id}", method = RequestMethod.POST)
	public ResponseEntity<Tarefa> inserirAnexo(
			@RequestParam(value = "file", required = false) MultipartFile inputFileImg,@PathVariable Long id) {

		try {
			

			Tarefa tarefa = new Tarefa();
			if (inputFileImg != null) {
				try {
					System.out.println("QUANTIDADE>>>>>>>>>> " +inputFileImg.getSize());
					tarefa = daoTarefa.buscar(id);
					List<String> files = new ArrayList<String>();
					Path rootLocation = Paths.get(servletContext.getRealPath("/"));
					Files.copy(inputFileImg.getInputStream(), rootLocation.resolve(inputFileImg.getOriginalFilename()));
					files.add(inputFileImg.getOriginalFilename());
					List<Anexos> listanexos = new ArrayList<>();
					for (String caminho : files) {
						Anexos anexo = new Anexos();
						anexo.setCaminho(caminho);
						
						listanexos.add(anexo);
					}
					tarefa.getAnexos().addAll(listanexos);
					daoTarefa.alterar(tarefa);
					/*
					 * 
					 
					// Cria os caminhos que ser�o salvos as imagens
					String caminhoDiretorio = servletContext.getRealPath("/");
					String caminhoimagem = "/empresa" + id + ".jpg";

					// cria um arquivo de destino para transferir a imagem para
					// o servidor.
					File destinationFile = new File(caminhoDiretorio + caminhoimagem);

					// testa se a extens�o da imagem � do tipo jpg ou jpeg se
					// n�o
					// for retorna um codigo de erro.
					/*
					if (!(inputFileImg.getContentType().contains("png")
							|| inputFileImg.getContentType().contains("jpg"))) {
						return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
					}
					

					// transfere a imagem para o diret�rio no servidor
					inputFileImg.transferTo(destinationFile);

					// busca a empresa e coloca o caminho da imagem no banco de
					// dados
					Anexos imagem = new Anexos();
					imagem.setCaminho(caminhoimagem);
					tarefa = daoTarefa.buscar(id);
					tarefa.getAnexos().add((imagem));
					daoTarefa.alterar(tarefa);
*/
					// return new ResponseEntity<Empresa>(HttpStatus.OK);
					return ResponseEntity.created(new URI("/empresa/" + tarefa.getId())).body(tarefa);

				} catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<Tarefa>(HttpStatus.BAD_REQUEST);
				}
			} else {
				System.out.println("AQUIIIIIIIIIIIIII");
				return ResponseEntity.created(new URI("/empresa/" + tarefa.getId())).body(tarefa);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	
	@RequestMapping(value = "/tarefa/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Tarefa> criarTarefa(@RequestBody Tarefa tarefa,HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			JWTVerifier verifier = new JWTVerifier(TokenJWT.SECRET);
			Map<String, Object> claims = verifier.verify(token);

			String idString = claims.get("id_usuario").toString();

			Long id = Long.parseLong(idString);
			Usuario usuario = daoUsuario.buscar(id);

			daoTarefa.inserir(tarefa);

			usuario.getTarefas().add(tarefa);
			daoUsuario.alterar(usuario);
			return ResponseEntity.created(URI.create("/tarefa/" + tarefa.getId())).body(tarefa);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	@RequestMapping(value = "/tarefa/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Tarefa> buscarTarefa(@PathVariable Long id) {
		try {
			Tarefa tarefaBuscada = daoTarefa.buscar(id);
			return ResponseEntity.status(HttpStatus.OK).body(tarefaBuscada);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/tarefa/", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Tarefa> alterarTarefa(@RequestBody Tarefa tarefa) {
		try {
			daoTarefa.alterar(tarefa);
			return ResponseEntity.status(HttpStatus.OK).body(tarefa);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "tarefa/lista/", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<List<InfoTarefa>> buscarTodasTarefas(HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			JWTVerifier verifier = new JWTVerifier(TokenJWT.SECRET);
			Map<String, Object> claims = verifier.verify(token);

			String idString = claims.get("id_usuario").toString();

			Long idUsuario = Long.parseLong(idString);
			Usuario usuarioBuscado = daoUsuario.buscar(idUsuario);
			if (usuarioBuscado.getTarefas() != null) {
				List<InfoTarefa> infoTarefas = new ArrayList<InfoTarefa>();
				List<Tarefa> tarefas = usuarioBuscado.getTarefas().stream().sorted(Comparator.comparing(Tarefa::getPrioridade)).collect(Collectors.toList());
				for (Tarefa tarefa : tarefas) {
					InfoTarefa infoTarefa = new InfoTarefa();
					infoTarefa.setTarefa(tarefa);
					if (tarefa.isStatus() != false) {
						infoTarefa.setStatus("Done");
					}else{
						infoTarefa.setStatus("Processada");
					}
					infoTarefa.setNomeUsuario(usuarioBuscado.getNome());
					infoTarefas.add(infoTarefa);
				}
				infoTarefas.stream().sorted(Comparator.comparing(InfoTarefa::getStatus).reversed()).collect(Collectors.toList());
				return ResponseEntity.status(HttpStatus.OK).body(infoTarefas);	
			}else{
				return ResponseEntity.status(HttpStatus.OK).body(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<InfoTarefa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/tarefa/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletarTarefa(@PathVariable Long id,HttpServletRequest request) {
		try{
			Tarefa tarefa = daoTarefa.buscar(id);
			String token = request.getHeader("Authorization");
			JWTVerifier verifier = new JWTVerifier(TokenJWT.SECRET);
			Map<String, Object> claims = verifier.verify(token);

			String idString = claims.get("id_usuario").toString();

			Long idUsuario = Long.parseLong(idString);
			Usuario usuario = daoUsuario.buscar(idUsuario);
			
			if (usuario.getTarefas().size()>0) {
				int i =0;
				for (Tarefa tarefaAserBuscada : usuario.getTarefas()) {
					if (tarefaAserBuscada.getId() == tarefa.getId()) {
						break;
					}
					i++;
				}
				usuario.getTarefas().remove(i);
			}
			
			//usuario.getTarefas().remove(tarefa.getId());
			daoUsuario.alterar(usuario);
			return ResponseEntity.noContent().build();
		}catch(Exception e){
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

}
