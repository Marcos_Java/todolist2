package desafio.controller;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import desafio.dao.DAOUsuario;
import desafio.model.TokenJWT;
import desafio.model.Usuario;
import desafio.view.InformacoesUsuario;

@RestController
@CrossOrigin
public class UsuarioController {
	@Autowired
	private DAOUsuario daoUsuario;

	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public ResponseEntity<Usuario> inserirUsuario(@RequestBody Usuario usuario) {
		try {
			daoUsuario.inserir(usuario);
			return ResponseEntity.created(URI.create("/usuario/" + usuario.getId())).body(usuario);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Usuario>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<InformacoesUsuario> logar(@RequestBody Usuario usuario, HttpServletRequest request) {

		usuario = daoUsuario.logar(usuario);
		if (usuario != null) {
			InformacoesUsuario informacoesUsuario = new InformacoesUsuario();
			TokenJWT tokenJwt = TokenJWT.gerarToken(usuario, request);
			informacoesUsuario.setToken(tokenJwt);
			informacoesUsuario.setUsuario(usuario);
			return ResponseEntity.ok(informacoesUsuario);
		} else {
			return new ResponseEntity<InformacoesUsuario>(HttpStatus.UNAUTHORIZED);
		}
	}
	/*
	 * Metodo busca o usuario pelo email
	 */
	@RequestMapping(value = "/buscar/usuario", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<InformacoesUsuario> buscarUsuario(@RequestBody Usuario usuario, HttpServletRequest request) {

		usuario = daoUsuario.buscarPorEmail(usuario);
		if (usuario != null) {
			InformacoesUsuario informacoesUsuario = new InformacoesUsuario();
			TokenJWT tokenJwt = TokenJWT.gerarToken(usuario, request);
			informacoesUsuario.setToken(tokenJwt);
			informacoesUsuario.setUsuario(usuario);
			return ResponseEntity.ok(informacoesUsuario);
		} else {
			return new ResponseEntity<InformacoesUsuario>(HttpStatus.BAD_REQUEST);
		}
	}
}
