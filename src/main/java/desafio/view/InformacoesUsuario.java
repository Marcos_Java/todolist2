package desafio.view;

import desafio.model.TokenJWT;
import desafio.model.Usuario;

public class InformacoesUsuario {

	private Usuario usuario;

	private TokenJWT token;

	public TokenJWT getToken() {
		return token;
	}

	public void setToken(TokenJWT token) {
		this.token = token;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
