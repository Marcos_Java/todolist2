package desafio.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.auth0.jwt.JWTVerifier;

import desafio.model.TokenJWT;

public class JWTInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		int n = 1;
		if (n == 1) {
			return true;
		}

		System.out.println("Estou FUNCIONANDO !!!!!!!!!!!!!!!!!!!!!");
		if (request.getMethod().equals("OPTIONS")) {
			System.out.println("DENTRO DO PRIMEIRO IF TRUE !!!!!!!!!!!!!!!!!!!!!");
			return true;
		}

		if (!(handler instanceof HandlerMethod)) {
			System.out.println("DENTRO DO SEGUNDO IF TRUE !!!!!!!!!!!!!!!!!!!!!");
			return true;
		}

		HandlerMethod methodInfo = (HandlerMethod) handler;

		if (methodInfo.getMethod().getName().equals("logar")
				|| methodInfo.getMethod().getName().equals("inserirUsuario")
				|| methodInfo.getMethod().getName().equals("buscarUsuario")) {
			return true;
		} else {
			String token = null;
			try {
				token = request.getHeader("Authorization");
				JWTVerifier verifier = new JWTVerifier(TokenJWT.SECRET);

				if (token == null) {
					System.out.println("DENTRO DO IF DO TOKEN TRUE !!!!!!!!!!!!!!!!!!!!!");
					return false;
				} else {
					System.out.println("DENTRO DO IF DO TOKEN FALSE !!!!!!!!!!!!!!!!!!!!!");
					return true;
				}

			} catch (Exception e) {
				e.printStackTrace();
				if (token == null) {
					response.sendError(HttpStatus.UNAUTHORIZED.value());
				} else {
					response.sendError(HttpStatus.FORBIDDEN.value());
				}
				return false;
			}
		}
	}

}
