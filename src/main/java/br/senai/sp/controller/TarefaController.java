package br.senai.sp.controller;

import java.net.URI;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.dao.DAOTarefa;
import br.senai.sp.dao.DAOUsuario;
import br.senai.sp.modelo.SubTarefa;
import br.senai.sp.modelo.Tarefa;
import br.senai.sp.modelo.Usuario;

@RestController
public class TarefaController {

	@Autowired
	private DAOTarefa daoTarefa;

	@Autowired
	private DAOUsuario daoUsuario;

	@RequestMapping(value = "/tarefa/{idUsuario}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Tarefa> criarTarefa(@RequestBody Tarefa tarefa, @PathVariable Long idUsuario) {
		try {
			for (SubTarefa subTarefa : tarefa.getSubTarefas()) {
				subTarefa.setTarefa(tarefa);
			}

			Usuario usuario = daoUsuario.buscar(idUsuario);
			tarefa.setUsuario(usuario);

			daoTarefa.inserir(tarefa);
			return ResponseEntity.created(URI.create("/tarefa/" + tarefa.getId())).body(tarefa);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Tarefa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "tarefa/lista/{idUsuario}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<List<Tarefa>> buscarTodasTarefas(@PathVariable Long idUsuario) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(daoTarefa.buscarTodos(idUsuario));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Tarefa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
