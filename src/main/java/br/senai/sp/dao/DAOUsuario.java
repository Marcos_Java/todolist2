package br.senai.sp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.modelo.Usuario;

@Repository
public class DAOUsuario {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Usuario usuario) {
		manager.persist(usuario);
	}

	public Usuario buscar(Long idUsuario) {
		return manager.find(Usuario.class, idUsuario);
	}

	public Usuario logar(Usuario usuario) {
		TypedQuery<Usuario> query = manager.createQuery("from Usuario where email = :email and senha = :senha",
				Usuario.class);
		query.setParameter("email", usuario.getEmail());
		query.setParameter("senha", usuario.getSenha());
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
