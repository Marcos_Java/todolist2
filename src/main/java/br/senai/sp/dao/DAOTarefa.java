package br.senai.sp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.modelo.Tarefa;
import br.senai.sp.modelo.Usuario;

@Repository
public class DAOTarefa {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Tarefa tarefa) {
		manager.persist(tarefa);
	}

	public Tarefa buscar(Long idTarefa) {
		return manager.find(Tarefa.class, idTarefa);
	}

	public List<Tarefa> buscarTodos(Long idUsuario) {
		TypedQuery<Tarefa> query = manager.createQuery("from Tarefa where usuario = :usuario", Tarefa.class);
		query.setParameter("usuario", manager.find(Usuario.class, idUsuario));
		return query.getResultList();
	}

	@Transactional
	public void excluir(Long idTarefa) {

		manager.remove(buscar(idTarefa));
	}

}
